package com.example.note.note;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class NoteService {

    private final NoteRepository noterepository;

    @Autowired
    public NoteService(NoteRepository noterepository){
        this.noterepository=noterepository;
    }
    public List<Note> getNotes(){
            return noterepository.findAll();
    }

    public List<Note> addNote(Note note) {
        noterepository.save(note);
        return noterepository.findAll();
    }

    public List<Note> deleteNote(Note note) {

        if (noterepository.existsById(note.getId())) {
            noterepository.delete(note);
        }
        return noterepository.findAll();
    }

    @Transactional
    public List<Note> updateNote(Note note) {

        if (noterepository.existsById(note.getId())) {
            noterepository.getReferenceById(note.getId()).setContent(note.getContent());
        }
        return noterepository.findAll();
    }
}
