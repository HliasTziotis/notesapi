package com.example.note.note;

import jakarta.persistence.*;

@Entity
@Table(name="notes")
public class Note {
    @Id
    @SequenceGenerator( name = "id_sequence",
                        sequenceName = "id_sequence",
                        allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "id_sequence")

    private Long id;
    private String content;

    public Note(){}

    public Note(Long id, String content) {
        this.id = id;
        this.content = content;
    }

    public Note(String content) {
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Note{" +
                "id=" + id +
                ", content='" + content + '\'' +
                '}';
    }
}
