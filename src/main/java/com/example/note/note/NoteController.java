package com.example.note.note;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(path = "/api/note")
public class NoteController {

    private final NoteService noteService;

    @Autowired
    public NoteController(NoteService noteService){
        this.noteService=noteService;
    }
    @GetMapping
    public List<Note> getNotes(){
        return noteService.getNotes();
    }
    @PostMapping
    public List<Note> registerNote(@RequestBody Note note){return noteService.addNote(note);
    }
    @DeleteMapping
    public List<Note> deleteNote(@RequestBody Note note){
            return noteService.deleteNote(note);
    }
    @PutMapping
    public List<Note> updateNote(@RequestBody Note note){
        return noteService.updateNote(note);
    }
}